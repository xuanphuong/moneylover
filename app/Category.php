<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'wallet_id'];

    /**
     * Get the Wallet record that belongs to the Category.
     */
    public function wallet() {
        return $this->belongsTo('App\Wallet');
    }
}
