<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use Auth;
use DB;
use App\Wallet;
use App\Category;

class CategoryController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::all();
        $wallets = Wallet::all();
        return view('categories.create', compact('wallets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        $category = new Category;
        $category->name = $request->category_name;
        $category->wallet_id = $request->wallet_id;

        $category->save();
        $request->session()->flash('success', 'New Category has been created!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        return view('categories.showCategories', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::all();
        $category = Category::findOrFail($id);
        $wallet = Wallet::findOrFail($category->wallet_id);
        return view('categories.edit', compact('category', 'wallet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->category_name_edit;
        $category->wallet_id = $request->wallet_id_edit;
        // $category->save();
        DB::table('categories')
            ->where('id', $id)
            ->update(array('name' => "$category->name"));
        $request->session()->flash('success', 'The Category has been updated!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return back()->withSuccess("The '$category->name' Wallet has been deleted.");
    }

    public function showCategory($id)
    {
        $wallet = Wallet::findOrFail($id);
        $wallet->with('categories');
        return view('categories.showCategories', compact('wallet'));
    }
}
