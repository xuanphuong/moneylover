<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Wallet;
use App\Http\Requests;
use Auth;
use DB;

class WalletController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => ['index', 'show', 'edit']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wallets = Wallet::all()->where('user_id', Auth::user()->id);
        return view('wallets.index', ['wallets' => $wallets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::all();

        return view('wallets.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        $wallet = new Wallet;
        $wallet->name = $request->wallet_name;
        $wallet->balance = $request->wallet_balance;
        $wallet->user_id = $user_id ;

        $wallet->save();
        $request->session()->flash('success', 'New Wallet has been created!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $wallet = Wallet::find($id);

        return view('wallets.show', compact('wallet'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wallet = Wallet::findOrFail($id);
        // if($wallet->user_id == Auth::user()->id) {
            return view('wallets.edit', compact('wallet'));
        // }
        // return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $wallet = Wallet::find($id);
        $wallet->name = $request->edit_name_wallet;
        $wallet->balance = $request->edit_balance_wallet;
        $wallet->info = $request->edit_info_wallet;
        
        $wallet->save();
        $request->session()->flash('success', 'The Wallet has been updated!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wallet = Wallet::findOrFail($id);
        $wallet->delete();

        return back()->withSuccess("The '$wallet->name' Wallet has been deleted.");
    }

    /**
     * Set Current Wallet
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function setCurrent(Request $request, $id) {
        $user = Auth::user();
        $user->last_wallet = $id;
        if($user->last_wallet != 0) {
            $last_wallet = Wallet::find($user->last_wallet);
            $last_wallet->is_current = 0;
        }
        
        $wallets = DB::table('wallets')->where('user_id', $user->id)->where('is_current', '=', 1)->update(array('is_current' => 0));
        
        $wallet = Wallet::findOrFail($id);
        
        $wallet->is_current = 1;
        $wallet->save();
        $user->save();
        $request->session()->flash('success', 'The Wallet has been set current');
        // return back();
        return view('wallets.set-current', compact('wallet'));
    }


    /**
     * Get all Wallet as User
     *
    */
    public function transferMoney() {
        $wallets = Wallet::all()->where('user_id', Auth::user()->id);
        return view('wallets.transfer', ['wallets' => $wallets]);
    }

    /**
     * Transfer money between wallet
     *
    */
    public function transfer(Request $request) {
        // dd($request);
        if($request->from_wallet_id == 0 || $request->to_wallet_id == 0) {
            $request->session()->flash('errors', 'Please chose Wallet');
            return back();
        }
        if($request->from_wallet_id == $request->to_wallet_id) {
            $request->session()->flash('errors', 'Two Wallet must been differen');
            return back();
        }
        $from_wallet = Wallet::findOrFail($request->from_wallet_id);
        $to_wallet = Wallet::findOrFail($request->to_wallet_id);
        $from_wallet->balance = $from_wallet->balance - $request->amount;
        $to_wallet ->balance = $to_wallet ->balance + $request->amount;
        $request->session()->flash('success', 'The Money hase been transfer success');
        $from_wallet->save();
        $to_wallet->save();
        return back();
    }
}
