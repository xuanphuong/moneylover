<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('user/activation/{token}', 'Auth\AuthController@activateUser')->name('user.activate');
Route::group(['middleware' => 'auth'], function () {
	Route::resource('users', 'UserController');

	Route::resource('wallets', 'WalletController');
	Route::get('transfer', 'WalletController@transferMoney');
	Route::post('transfer', 'WalletController@transfer');

	Route::get('wallets/{wallet}/set_current', 'WalletController@setCurrent');

	Route::resource('categories', 'CategoryController');

	Route::get('wallets/{id}/categories', 'CategoryController@showCategory');
});
