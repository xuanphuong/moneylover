<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'balance', 'user_id'];

    /**
     * Get the User record that belongs to the Wallet.
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the Category record that has many to the Wallet.
     */
    public function categories() {
        return $this->hasMany('App\Category');
    }
}
