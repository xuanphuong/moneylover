@extends('layouts.app')

@section('content')

    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1>Personal expense manager</h1>
                <hr>
                <p id="solution">Solution Manger Money</p>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Getting Started</a>
            </div>
        </div>
    </header>   

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Let's Get In Touch!</h2>
                    <hr class="primary">
                    <p>Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x wow bounceIn"></i>
                    <p>0979761430</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
                    <p><a href="mailto:xuanphuong3105@gmail.com">xuanphuong3105@gmail.com</a></p>
                </div>
            </div>
        </div>
    </section>
@endsection
