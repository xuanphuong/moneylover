
<div class="panel-body">
    @include('admin.partials.errors')
    @include('admin.partials.success')
    <form action="{{ url('wallets/' . $wallet->id) }}" method="POST" class="form-horizontal" role="form">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Name</label>
            <div class="col-md-8 input-group">
                <span class="input-group-addon">@</span>
                <input type="text" class="form-control" id="name" name="edit_name_wallet" value="{{ $wallet->name }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Balance</label>
            <div class="col-md-8 input-group">
                <span class="input-group-addon">@</span>
                <input type="number" class="form-control" id="name" name="edit_balance_wallet" value="{{ $wallet->balance }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Info</label>
            <div class="col-md-8 input-group">
                <span class="input-group-addon">@</span>
                <textarea class="form-control" id="name" name="edit_info_wallet" value="">{{ $wallet->info }}</textarea>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-xm-5 col-sm-offset-4">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
            
        </div>
    </form>
</div>
