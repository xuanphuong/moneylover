
<div class="panel-body">
    @include('admin.partials.errors')
    @include('admin.partials.success')
    <form action="{{ url('wallets/' . $wallet->id) }}" method="POST" class="form-horizontal" role="form">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <input type="hidden" class="form-control" id="name" name="edit_name_wallet" value="{{ $wallet->name }}">
        </div>

        <div class="form-group">
            <input type="hidden" class="form-control" id="name" name="edit_balance_wallet" value="{{ $wallet->balance }}">
        </div>

        <div class="form-group">
            <input type="hidden" class="form-control" id="name" name="edit_info_wallet" value="{{ $wallet->info }}">
        </div>
        <div class="form-group">
            <div class="col-xm-5 col-sm-offset-8">
                <button type="submit" class="btn btn-primary">Oke</button>
                <!-- <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> -->
            </div>
        </div>
    </form>
</div>
