@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    
        <div class="col-md-10 col-md-offset-1">
        	@if (Auth::check())
		        <div class="row">
		            <div class="col-md-6">
		                <a href="{{ url('wallets/create') }}"><button class="btn btn-success"><i class="fa fa-plus-square"></i> New Wallet</button></a>
		            </div>
		        </div>
		        <hr/>
		    @endif
            <div class="panel panel-default">
                <div class="panel-heading">List Wallet</div>
                <div class="panel-body">
					@include('admin.partials.errors')
                    @include('admin.partials.success')
					<table class="table table-bordered table-hover" id="list-wallet">
						<thead>
							<tr>
								<th>Wallet Name</th>
								<th>Wallet Balance</th>
								<th>Wallet Current</th>
								<th>View</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							@if(isset($wallets))
						        @foreach ($wallets as $wallet)
						            <tr>
						                <td>{{$wallet->name or ''}}</td>
						                <td>{{$wallet->balance}}</td>
						                <td>
						                	<button class="btn btn-info load-form-modal" {{$wallet->is_current== 1 ? 'disabled':''}} data-url="{{url('wallets'). '/' . $wallet['id'].'/set_current'}}" data-toggle ="modal" data-target='#form-modal' >{{ Form::radio('current-wallet', $wallet->id, ($wallet->is_current== 1 ? 'checked="checked"':'')) }} Current
						                	</button>
						            	</td>
										<td>
											<a href="{{ url('wallets/' . $wallet->id ) }}"><button type="button" class="btn btn-success"><i class="fa fa-eye"></i> View</button></a>
										</td>
						                <!-- <td><a href="{{ url('wallets/' . $wallet->id . '/edit') }}"><button type="button" class="btn btn-warning"><i class="fa fa-edit"></i> Edit</button></a></td> -->
						               <td>
						               	 <button class="btn btn-info load-form-modal" data-url="{{url('wallets'). '/' . $wallet['id'].'/edit'}}" data-toggle ="modal" data-target='#form-modal' ><i class="fa fa-edit"></i>
                        					Edit
                    					</button>

						               </td>
						                <td>
						                	<button class="btn btn-danger load-confirmation-modal" data-url="{{url('wallets/' . $wallet->id )}}" data-toggle ="modal" data-target='#confirmation-modal' ><i class="fa fa-trash"></i>
	                        					Delete
	                    					</button>
						                </td>
						            </tr>
						        @endforeach
					    	@endif
						</tbody>
					</table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection