@extends ('layouts.app')

@section ('content')
<!-- <div class="row"> -->
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Transfer Money</div>
            <div class="panel-body">
                @include('admin.partials.errors')
                @include('admin.partials.success')
                <form action="" method="POST" class="form-horizontal" role="form">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label for="department" class="col-md-4 control-label">From Wallet</label>
                        <div class="col-md-4 input-group" id='from-wallet'>
                            <select class="form-control" id="department" name="from_wallet_id" >
                                <option selected disabled value="0">Choose one</option>
                                @foreach ($wallets as $wallet)
                                    <option value="{{ $wallet->id }}">{{ $wallet->name . ' $' . $wallet->balance}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                     <div class="form-group">
                        <label for="department" class="col-md-4 control-label">To Wallet</label>
                        <div class="col-md-4 input-group" id='to-wallet'>
                            <select class="form-control" id="department" name="to_wallet_id">
                                <option selected disabled value="0">Choose one</option>
                                @foreach ($wallets as $wallet)
                                    <option value="{{ $wallet->id }}">{{ $wallet->name . ' $' . $wallet->balance}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Amount</label>
                        <div class="col-md-8 input-group">
                            <span class="input-group-addon">$</span>
                            <input type="number" min="0" class="form-control" id="name" name="amount">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Note</label>
                        <div class="col-md-8 input-group">
                            <span class="input-group-addon">@</span>
                            <!-- <input type="number" class="form-control" id="name" name="edit_balance_wallet" > -->
                            <textarea class="form-control" id="name" name="note" value="">{{ $wallet->info }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Date</label>
                        <div class="col-md-8 input-group">
                            <span class="input-group-addon">@</span>
                            <input type="datetime" class="form-control" id="name" name="edit_balance_wallet" >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-xm-5 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@stop