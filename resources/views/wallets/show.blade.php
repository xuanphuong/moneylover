@extends ('layouts.app')

@section ('content')
<div class="container">
	<div class="row">
    	<div class="col-md-8 col-md-offset-2">
        	<div class="panel panel-default">
            	<div class="panel-heading">Wallet</div>
            	<div class="panel-body">
					<div class="col-xs-7 col-sm-9">

						<table class="table table-hover table-bordered">
							<thead>
								<tr>
									<th>Wallet Name</th>
									<th>Wallet Balance</th>
									<th>Wallet Info</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ $wallet->name}}</td>
									<td>{{ $wallet->balance}}</td>
									<td>{{ $wallet->info}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop