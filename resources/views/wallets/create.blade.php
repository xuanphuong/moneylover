@extends ('layouts.app')

@section ('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Create Wallet</div>
            <div class="panel-body">
                @include('admin.partials.errors')
                @include('admin.partials.success')
                <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('wallets/') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Wallet Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="wallet_name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Wallet Balance</label>
                        <div class="col-md-6">
                            <input type="number" class="form-control" name="wallet_balance">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-wallet"></i>Create Wallet
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop