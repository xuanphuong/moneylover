@if(Session::has('errors'))
    <div class="alert alert-danger">
    	<button class="close" type="button" data-dismiss="alert">&times;</button>
        <strong>Whoops!</strong>
        There were some problems with your input.<br/><br/>
        {{ Session::get('errors') }}
    </div>
@endif