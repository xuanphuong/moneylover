
<div class="panel-body">
    @include('admin.partials.errors')
    @include('admin.partials.success')
    <form action="{{ url('categories/' . $category->id) }}" method="POST" class="form-horizontal" role="form">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Name</label>
            <div class="col-md-8 input-group">
                <span class="input-group-addon">@</span>
                <input type="text" class="form-control" id="name" name="category_name_edit" value="{{ $category->name }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="name">Wallet</label>
            <div class="col-md-8 input-group">
                <select class="form-control" id="name" name="wallet_id_edit" disabled>
                    <option value="{{ $wallet->id }}">{{ $wallet->name }}</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xm-5 col-sm-offset-4">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </form>
</div>
