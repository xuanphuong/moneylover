@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">List Category</div>
                <div class="panel-body">
					@include('admin.partials.errors')
                    @include('admin.partials.success')
					<table class="table table-bordered table-hover" id="list-wallet">
						<thead>
							<tr>
								<th>Category Name</th>
								<th>Wallet Name</th>
								<th>Edit</th>
							</tr>
						</thead>
						<tbody>
							@if(isset($wallet))
						        @foreach ($wallet->categories as $category)
						            <tr>
						                <td>{{$category->name or ''}}</td>
						                <td>{{$wallet->name}}</td>
						                <td>
						                	<button class="btn btn-info load-form-modal" data-url="{{url('categories'). '/' . $category['id'].'/edit'}}" data-toggle ="modal" data-target='#form-modal' ><i class="fa fa-edit"></i>
                        							Edit
                							</button>
                    					</td>
						            </tr>
						        @endforeach
					    	@endif
						</tbody>
					</table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection