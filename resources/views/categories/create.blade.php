@extends ('layouts.app')

@section ('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Create Category</div>
            <div class="panel-body">
                @include('admin.partials.errors')
                @include('admin.partials.success')
                <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('categories/') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Category Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="category_name">
                        </div>
                    </div>

                   <div class="form-group">
                        <label for="department" class="col-lg-3 control-label">Wallet</label>
                        <div class="col-lg-8">
                            <select class="form-control" id="department" name="wallet_id">
                                @foreach ($wallets as $wallet)
                                    <option value="{{ $wallet->id }}">{{ $wallet->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-wallet"></i>Create Category
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop