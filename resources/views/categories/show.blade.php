@extends ('layouts.app')

@section ('content')
<div class="container">
	<div class="row">
    	<div class="col-md-8 col-md-offset-2">
        	<div class="panel panel-default">
            	<div class="panel-heading">Category</div>
            	<div class="panel-body">
					<div class="col-xs-7 col-sm-9">

						<table class="table table-hover">
							<thead>
								<tr>
									<th>Category Name</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ $category->name}}</td>
									
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop