@extends('layouts.app')

@section('content')
	
<div class="container">
    <div class="row">

<div class="col-md-10 col-md-offset-1">
			@if (Auth::check())
		        <div class="row">
		            <div class="col-md-6">
		                <a href="{{ url('categories/create') }}"><button class="btn btn-success"><i class="fa fa-plus-square"></i> New Category</button></a>
		            </div>
		        </div>
		        <hr/>
		    @endif
            <div class="panel panel-default">
                <div class="panel-heading">List Category</div>
                <div class="panel-body">
					@include('admin.partials.errors')
                    @include('admin.partials.success')
                     @if (Auth::check())
						<table class="table table-bordered table-hover" id="list-categories">
							<thead>
								<tr>
									<th>Category Name</th>
									<th>Wallet Name</th>
									<th>Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($categories))
							        @foreach ($categories as $category)
							            <tr>
							                <td>
							                	<a href="{{url('wallets/'.$category->wallet->id . '/categories')}}" >{{$category->name}}</a>
							                </td>
							                <td>{{$category->wallet->name or ''}}</td>
						                   
					                        <td>
					                        	<button class="btn btn-info load-form-modal" data-url="{{url('categories'). '/' . $category['id'].'/edit'}}" data-toggle ="modal" data-target='#form-modal' ><i class="fa fa-edit"></i>
                        							Edit
                    							</button>
					                        </td>
					                        <td>
					                        	<button class="btn btn-danger load-confirmation-modal" data-url="{{url('categories/' . $category->id )}}" data-toggle ="modal" data-target='#confirmation-modal' ><i class="fa fa-trash"></i>
	                        					Delete
	                    						</button>
					                        </td>
							            </tr>
							        @endforeach
	    						@endif

							</tbody>
						</table>
                    @endif
                </div>
            </div>
        </div>
	</div>
</div>


@endsection