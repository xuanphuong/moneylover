@extends ('layouts.app')

@section ('content')
<div class="container">
	<div class="row">
    	<div class="col-md-8 col-md-offset-2">
        	<div class="panel panel-default">
            	<div class="panel-heading">Profile</div>
            	<div class="panel-body">
					<div class="col-xs-6 col-sm-3">
					    <img class="img-responsive" alt="Image" src="/{{ $user->photo != '' ? $user->photo : 'images/avatar.png'}}" id="avatar">
					</div>
					<div class="col-xs-7 col-sm-9">
						<div><h4>Name: {{ $user->name}}</h4></div>
						<div><h4>Email: {{ $user->email}}</h4></div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
@stop