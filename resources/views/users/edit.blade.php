@extends ('layouts.app')

@section ('content')
<div class="">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Profile</div>
            <div class="panel-body">
                @include('admin.partials.errors')
                @include('admin.partials.success')
                <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('users/'.$user->id) }}">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <!-- Upload Image -->
                    <div class="form-group col-md-5">
                        <div class="col-sm-4 col-md-12 col-xs-6">
                            <label class="col-md-4 control-label" for="avatar">Avatar</label>
                            <img src="/{{ ($user->photo != '' ? $user->photo : 'images/avatar.png')}}" id="avatar" class="img-responsive" alt="Employee Photo">
                            <div class="form-group">
                                <label for="photo" class="control-label"></label>
                                <input type="file" id ="photo" class="form-control" name="photo" onchange="readURL(this);" />
                                <script>
                                    function readURL(input) {
                                        if (input.files && input.files[0]) {
                                        var reader = new FileReader();

                                        reader.onload = function (e) {
                                        $('#avatar')
                                        .attr('src', e.target.result)
                                        //                    .width(200)
                                        //                    .height(200);
                                        };

                                        reader.readAsDataURL(input.files[0]);
                                        }
                                    }
                                </script>
                            </div>
                        </div>
                    </div>    
                    <!-- End Upload Image -->
                    <div class="col-md-7">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Name</label>

                        <div class="col-md-8 input-group">
                            <span class="input-group-addon">@</span>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="email">E-Mail Address</label>

                        <div class="col-md-8 input-group">
                            <span class="input-group-addon">@</span>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i>Update Profile
                            </button>
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop