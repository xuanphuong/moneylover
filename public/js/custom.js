$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});
$(function(){
	$('#list-wallet').DataTable({
		reponsive: true,
		ordering: true,
        "lengthMenu": [ [ 5, 25, 50, 75, 100, -1], [ 5, 25, 50, 75, 100, "All"] ],
        "columnDefs": [
            { "orderable": false, "targets": [3,4,5,6] },
            { "searchable": false, "targets": [3,4,5,6] },
        ]
	});
    $('#list-categories').DataTable({
        reponsive: true,
        ordering: true,
    });
});
$("a.dropdown-toggle").attr("aria-expanded","true");

/* Load Modal Bootstrap */
var Modal = {
    init: function () {
        this.initEditModal();
        this.initConfirmationModal();
    },
    initEditModal: function() {
        $(document).on('click', '.load-form-modal', function(event){
            console.log('Modal: '+ $(this).attr('data-url'));
            $('#form-modal .modal-body').load($(this).attr('data-url'));
            event.preventDefault();
        });
    },
    initConfirmationModal: function() {
        $(document).on('click', '.load-confirmation-modal', function(event){
            $('#confirmation-modal form').attr('action', $(this).attr('data-url'));
            event.preventDefault();
        });
    }
};

Modal.init();

/* Flash time out */
$('div.alert').delay(2000).fadeOut(500);

// $(function() {
//     $('form').on('submit', function( event ) {
//         if( $(':selected')) {
//             if( $('select[name=from_wallet_id]').val() == $('select[name=to_wallet_id]').val()) {
//                 alert( 'Please ensure all fields are selected to submit the form' );
//             }
//         }
//     });
// });